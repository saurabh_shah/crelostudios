const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const port = process.env.PORT || 8000;
const db = require("./database/config");
require("dotenv").config();
const app = express();
const fs = require("fs");

app.use(cors());

app.use(bodyParser.urlencoded({ limit: "200mb", extended: false }));
app.use(bodyParser.json());

let dir = "public";
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir, { recursive: true });
}

db.connection().then((database) => {
  module.exports = database;

  app.use("/api", require("./Route/user"));
  app.listen(port, () => {
    console.log(`Server Connected on port ${port} `);
  });
});
