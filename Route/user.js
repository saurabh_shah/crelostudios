const router = require("express").Router();
const userCrtl = require("../controller/user");
const { upload } = require("../helper/fileupload");
const passport = require("passport"),
  LocalStrategy = require("passport-local").Strategy;

// User Register  Api
router.post("/signup", userCrtl.signUp);

// User login Api
router.post(
  "/login",
  //   passport.authenticate("local", {
  //     successRedirect: "/api",
  //     failureRedirect: "/login",
  //     failureFlash: true,
  //   }),

  userCrtl.userLogin
);

// Single image upload api-
router.post("/single-upload", upload.single("file"), userCrtl.singleUpload);

// To change Password
router.put("/changePassword", userCrtl.changePassword);

// To view profile details
router.get("/viewProfile", userCrtl.viewProfile);

// To insert address and update
router.put("/addressbook", userCrtl.addressBook);

// To insert address and update
router.put("/logout", userCrtl.logOut);

module.exports = router;
