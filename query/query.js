exports.insert = (collection, query) => {
  return new Promise((resolve, reject) => {
    collection.insertOne(query, (err, result) => {
      if (err) {
        return reject({ message: "DB query Failed", error: err });
      } else {
        resolve(result);
      }
    });
  });
};

exports.findOne = (collection, query) => {
  return new Promise((resolve, reject) => {
    collection.findOne(query, (err, result) => {
      if (err) {
        return reject({ message: "DB query Failed", error: err });
      } else {
        resolve(result);
      }
    });
  });
};

exports.find = (collection, query) => {
  return new Promise((resolve, reject) => {
    collection.find(query).toArray((err, result) => {
      if (err) {
        return reject({ message: "DB query Failed", error: err });
      } else {
        resolve(result);
      }
    });
  });
};

exports.findOneAndUpdate = (collection, query, setParameters, getResponse) => {
  return new Promise((resolve, reject) => {
    collection.findOneAndUpdate(
      query,
      setParameters,
      getResponse,
      (err, result) => {
        if (err) {
          return reject({ message: "DB query Failed", error: err });
        } else {
          resolve(result);
        }
      }
    );
  });
};

exports.aggregate = (collection, query) => {
  return new Promise((resolve, reject) => {
    collection
      .aggregate(query)
      .sort({ createdAt: -1 })
      .toArray((err, result) => {
        if (err) {
          return reject({ message: "DB query Failed", error: err });
        } else {
          resolve(result);
        }
      });
  });
};
