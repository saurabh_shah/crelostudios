const passport = require("passport"),
  LocalStrategy = require("passport-local").Strategy;
let Users = db.collection("users");

module.exports = () => {
  passport.use(
    new LocalStrategy(function (username, password, done) {
      Users.findOne({ email: username }, function (err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, { message: "Incorrect username." });
        }
        if (!user.validPassword(password)) {
          return done(null, false, { message: "Incorrect password." });
        }
        return done(null, user);
      });
    })
  );
};
