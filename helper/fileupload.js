const multer = require("multer");
const s3 = require("../aws/uploadconfig");
const dotenv = require("dotenv");
dotenv.config();
const path = require("path");
const bucketName = process.env.BUCKET_NAME;

// Upload to s3 bucket
const uploadToS3Bucket = async (fileContent, file) => {
  return new Promise(async (resolve, reject) => {
    let res = await s3
      .upload({
        Bucket: bucketName,
        Key: file,
        Body: fileContent,
        ACL: "public-read",
      })
      .promise();
    resolve(res);
  });
};

// Local upload
const upload = multer({
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "public/");
    },
    filename: function (req, file, cb) {
      let filename = path.parse(file.originalname).name;
      let time = new Date().getTime();
      let extension = path.extname(file.originalname);
      cb(null, filename + "-" + time + extension);
    },
  }),
});

module.exports = {
  uploadToS3Bucket,
  upload,
};
