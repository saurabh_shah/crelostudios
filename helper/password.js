const jwt = require("jsonwebtoken");

const generateOTP = () => {
  const digits = "123456789";
  let otp = "";
  for (let i = 1; i <= 6; i++) {
    let index = Math.floor(Math.random() * digits.length);
    otp = otp + digits[index];
  }
  return otp;
};

const generateExpireDate = (mins) => {
  const date = new Date();
  return date.setMinutes(date.getMinutes() + mins);
};

const generateJWTAccessToken = (user) => {
  return jwt.sign(
    {
      _id: user._id,
      email: user.email,
      type: user.type,
    },
    process.env.TOKEN_SECRET
  );
};

module.exports = {
  generateOTP,
  generateExpireDate,
  generateJWTAccessToken,
};
