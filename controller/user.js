const db = require("../server");
let Users = db.collection("users");
let query = require("../query/query");
const { ObjectId } = require("mongodb");
const { sendEmail } = require("../helper/sendmail");
const CryptoJS = require("crypto-js");
const {
  generateOTP,
  generateExpireDate,
  generateJWTAccessToken,
} = require("../helper/password");
require("dotenv").config();
const fs = require("fs");
const { uploadToS3Bucket } = require("../helper/fileupload");

// Register user by email,gender,name,password
const signUp = async (req, res, next) => {
  try {
    let data = req.body;
    let user = await query.findOne(Users, { email: data.email });

    // Checks if user already used Email Id or not
    if (user) {
      res.json({
        message: "Email Id already registered",
      });
    } else {
      // generate otp for email verification
      const otp = generateOTP();
      const message = `Hello,\nYou are receiving this email because your email needs to be verified. Your otp for verification is\n${otp}`;
      // send email
      const emailId = data.email;
      const subject = "Email Verification";
      await sendEmail(emailId, subject, message);
      data.otp = otp;
      data.otpExpire = generateExpireDate(10);
      const password = await CryptoJS.AES.encrypt(
        data.password,
        process.env.TOKEN_SECRET
      ).toString();
      data.password = password;
      console.log("data", data);
      let user = await query.insert(Users, data);
      delete data["otp"];
      res.json({
        message: "User registered successfully",
        data: user.ops[0],
      });
    }
  } catch (error) {
    res.json({
      data: error,
    });
  }
};

// Login Api
const userLogin = async (req, res, next) => {
  try {
    let { emailId, password } = req.body;

    const message = "Invalid login credentials.";

    let user = await query.findOne(Users, { email: emailId });
    // Checks valid email Id or not
    if (!user) {
      res.json({
        message: message,
      });
    }
    let isMatched = CryptoJS.AES.decrypt(
      user.password,
      process.env.TOKEN_SECRET
    );

    let userpassword = isMatched.toString(CryptoJS.enc.Utf8);

    let token = await generateJWTAccessToken(user);

    // checks password and update userstatus to 1
    if (userpassword == password) {
      await query.findOneAndUpdate(
        Users,
        { email: emailId },
        {
          $set: {
            userStatus: 1,
          },
        },
        { returnOriginal: false }
      );
      res.json({
        message: "Login successfully",
        token: token,
      });
    } else {
      res.json({
        message: message,
      });
    }
  } catch (error) {
    res.json({
      data: error,
    });
  }
};

// Single image upload api
const singleUpload = async (req, res, next) => {
  const file = req.file;
  const emailId = req.body.email;
  const mainDirectory = "public";
  let location = "";
  if (!file) {
    let message = "No file selected";
    res.json({
      message,
    });
  }

  console.log("file", `${mainDirectory}/${file.filename}`);
  // fetching local storage path in response
  location = `${mainDirectory}/${file.filename}`;
  let user = await query.findOneAndUpdate(
    Users,
    { email: emailId },
    {
      $set: {
        profilePic: location,
      },
    },
    { returnOriginal: false }
  );
  console.log("user", user);
  let response = { file: location };
  res.json({
    data: response,
  });

  // This is to upload file read and upload to s3 bucket
  // read image conents
  const fileContent = fs.readFileSync(`${mainDirectory}/${file.filename}`);
  // upload image to s3 bucket
  const mainImage = await uploadToS3Bucket(
    fileContent,
    `profile/${file.filename}`
  );
  location = mainImage.Location;
  // remove file
  fs.unlinkSync(`${mainDirectory}/${file.filename}`);
  // send response
  response = { file: location };
  res.json({
    data: response,
  });
};

//View user Profile
const viewProfile = async (req, res, next) => {
  try {
    const emailId = req.body.email;
    let userInfo = await query.find(Users, { email: emailId });

    delete userInfo[0].otp;
    delete userInfo[0].otpExpire;
    delete userInfo[0].password;

    res.json({
      message: "User details fetched successfully",
      data: userInfo,
    });
  } catch (error) {
    res.json({
      data: error,
    });
  }
};

// change user password
const changePassword = async (req, res, next) => {
  let message = "";
  const { oldPassword, newPassword, emailId } = req.body;

  // match old password
  let user = await query.findOne(Users, { email: emailId });
  console.log("user", user);
  var isMatched = CryptoJS.AES.decrypt(user.password, process.env.TOKEN_SECRET);

  let userpassword = isMatched.toString(CryptoJS.enc.Utf8);
  console.log("userpassword", userpassword);
  if (userpassword != oldPassword) {
    message = "Mismatched old password";
    res.json({
      message,
    });
  } else {
    // save new password
    const passwordEncrypt = await CryptoJS.AES.encrypt(
      newPassword,
      process.env.TOKEN_SECRET
    ).toString();
    await query.findOneAndUpdate(
      Users,
      { email: req.body.emailId },
      { $set: { password: passwordEncrypt } },
      { returnOriginal: false }
    );

    // send email of change password
    message = "Your password changed successfully.";
    const emailId = req.body.emailId;
    const subject = "Change password";
    await sendEmail(emailId, subject, message);

    // send response
    res.json({
      message,
    });
  }
};

//Insert Multiple address and update
const addressBook = async (req, res, next) => {
  let address = req.body.addressArray;
  let emailId = req.body.email;
  let user = await query.findOne(Users, { email: emailId });
  console.log("user", user.address);

  let result = await query.findOneAndUpdate(
    Users,
    { email: emailId },
    {
      $set: {
        address: address,
      },
    },
    { returnOriginal: false }
  );
  res.json({
    message: "Updated successfully",
    data: result.value,
  });
};

// User logout Api
const logOut = async (req, res, next) => {
  let emailId = req.body.email;
  await query.findOneAndUpdate(
    Users,
    { email: emailId },
    {
      $set: {
        userStatus: 0,
      },
    },

    { returnOriginal: false, new: true }
  );
  res.json({
    message: "User logout successfully",
  });
};

module.exports = {
  signUp,
  userLogin,
  singleUpload,
  viewProfile,
  changePassword,
  addressBook,
  logOut,
};
